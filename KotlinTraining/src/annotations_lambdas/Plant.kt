package annotations_lambdas

import kt_essentials.programflow.functions.formatMsgAnswerStr
import kotlin.reflect.*
import kotlin.reflect.full.declaredMemberFunctions

annotation class ImAPlant

@Target(AnnotationTarget.PROPERTY_GETTER)
annotation class OnGet
@Target(AnnotationTarget.PROPERTY_SETTER)
annotation class OnSet

@ImAPlant class Plant {
    @get:OnGet
    val isGrowing: Boolean = true
    @set:OnSet
    var needsFood: Boolean = false

    fun trim() {}
    fun fertilizer() {}
}

fun testAnnotation() {
    val clsObj: KClass<Plant> = Plant::class
    for (m: KFunction<*> in clsObj.declaredMemberFunctions) {
        formatMsgAnswerStr(msg = "Without Annotation", answer = m.name)
    }
}

fun testAnnotations2() {
    val plantObj: KClass<Plant> = Plant::class
    for (a: Annotation in plantObj.annotations) {
        val sName: String? = a.annotationClass.simpleName
        formatMsgAnswerStr("With Annotation", sName)
    }
}

fun labels() {
    outerloop@ for (i: Int in 1..100) {
        print("$i ")
        for (j: Int in 1..100) if (i > 10) break@outerloop
    }
    println()
}
// ------------------------------------------------------------
// ------------------------------------------------------------
data class Fish(var name: String)

val waterFilter: (Int) -> Unit = { dirty: Int ->
    formatMsgAnswerStr("Lambda: waterFilter", dirty / 2)
}

fun higherOrderMyWithFunc(
    name: String, block: String.() -> Unit) = name.block()

fun higherOrderFishFunc() {
    val fish = Fish(name = "splashy")
    val fish2 = Fish(name = "mysplashy")
    // Lambda
    with(receiver = fish.name) {
        formatMsgAnswerStr("Capitalizing the fish name", capitalize())
    }
    higherOrderMyWithFunc(name = fish2.name) {
        formatMsgAnswerStr("Capitalizing fish name with my\n    Higher Order Function",
                           capitalize())
    }
}

fun myFishFilterLambda() {
    val myFish: List<Fish> = listOf(
        Fish(name = "Flipper"),
        Fish(name = "Moby-Dick"),
        Fish(name = "Dory")
    )
    val nContains: List<Fish> = myFish
        .filter { it.name.contains(char = 'i') }
    formatMsgAnswerStr("Names with char('i')", nContains)
}
/** -------------------------------------------------------------------
 *  [Note]: [run()] and [apply()] are similar, but [run()] [returns]
 *  the result of applying the function, and [apply()] [returns]
 *  the [object] after applying the function.
 *  -----------------------------------------------------------------*/
fun runFIshLambda() {
    val runFish: String = Fish(name = "run.Splashy").run { name }
    formatMsgAnswerStr("Using the '.run':Lambda", runFish)
}

fun applyFishLambda() {
    val applyFish: Fish = Fish(name = "Splashy")
        .apply { name = "Sharky" }
    formatMsgAnswerStr("Using the '.apply':Lambda &\n" +
                       "    changing the name = Splashy", applyFish)
}

