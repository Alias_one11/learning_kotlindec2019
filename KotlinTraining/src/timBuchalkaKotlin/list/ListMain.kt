package timBuchalkaKotlin.list

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.*
import timBuchalkaKotlin.classes.*

fun main()
{
    //==============================================
    "List".codeDiv()
    //==============================================
    "List-Example#1".printHeader()
    "List of weapons".section()

    val nas = Player("<Nas>", 3, 5, 1000)
    val axe = Weapon("Axe", 12)
    nas.weapon = axe
    nas.weaponEquip()

    // Creating some potion for our player
    val redPotion = Loot("Red Potion", LootType.POTION, 7.50)
    // Adding armor to our player
    val chestArmor = Loot("Chest Armor+3", LootType.ARMOR, 80.0)
    val stock: ArrayList<Loot> = nas.inventory
        stock.apply {
            add(redPotion)
            add(chestArmor)
        }

    nas.showInventory()
    nas.toString()

    //==============================================
    "List-Example#2".printHeader()
    "Adding loot without a variable to store in".section()

    // Adding more loot to nas bag
    stock.apply {
        add( Loot("Ring Of Protection", LootType.RING, 40.25) )
        add( Loot("Invisibility-Potion", LootType.POTION, 35.95) )
    }

    nas.showInventory()

    //==============================================
    "List-Example#3".printHeader()

    "Encapsulation & Private Properties".section()
    // Dropping the redPotion from our loot inventory
    nas.dumped(redPotion)

    // Creating a new potion
    val purplePotion = Loot("Purple-Potion", LootType.POTION, 7.50)
    // We will try to drop the potion but since
    // we did not add the purplePotion to our player
    // the else statement in our dump method will be activated
    nas.dumped(purplePotion)

    nas.toString()

    //==============================================
    "List-Example#4".printHeader()

    "Overloading-Functions".section()



}

