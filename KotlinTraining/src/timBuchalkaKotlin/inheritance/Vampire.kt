package timBuchalkaKotlin.inheritance

open class Vampire(_name: String): Enemy(_name, _hitPts = 20, _lives = 3) {

    /**[ENEMY]: function overriding
     *  and changed for the Vampire class */
    override fun takeDmg(dmg: Int) {
        super.takeDmg(dmg.div(2))
    }

}