package timBuchalkaKotlin.inheritance

import java.util.*


class VampireKing(_name: String): Vampire(_name) {

    /** To change the [VampireKing]'s hit points to [140]*/
    init {
        _hitPts = 140
    }

    override fun takeDmg(dmg: Int) {
        super.takeDmg(dmg.div(2))
    }

    // An escape method for our Enemy VampireKing
    // used in the quickExit() method
    private fun escape(): Boolean = when {
        (_lives < 2) -> true
        else -> false
    }

    fun quickExit(elReyVamp: VampireKing) {
        loop@ while (elReyVamp._lives > 0) {
            when {
                elReyVamp.dodges() -> continue@loop
            }
            when {
                elReyVamp.escape() -> {
                    println("[[$_name] has escaped!!!]")
                    break@loop
                }
                else -> elReyVamp.takeDmg(80)
            }
        }
    }

    private fun dodges(): Boolean {
        val rand = Random()
        val chance = rand.nextInt(6)

        return when {
            (chance > 3) -> {
                println("[[!!![$_name] dodges!!!]]")
                true
            }
            else -> false
        }
    }
}