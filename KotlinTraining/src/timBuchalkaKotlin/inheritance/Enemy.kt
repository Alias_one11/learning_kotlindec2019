package timBuchalkaKotlin.inheritance

/**[PARENT]: class*/
open class Enemy(internal val _name: String, var _hitPts: Int, var _lives: Int) {

    open fun takeDmg(dmg: Int) {
        val remainingHitPts: Int = _hitPts.minus(dmg)

        when {
            (remainingHitPts > 0)  -> {
                _hitPts = remainingHitPts

                println("<| ($_name) took ($dmg) points of damage, |>\n" +
                             " <| & has ($_hitPts) remaining |>\n")
            }
            else -> {
                _lives = _lives.minus(1)
                when {
                    (_lives > 0) -> println("<[💀!!! ($_name) lost a life... !!!💀]>")
                    else -> println("$_name is dead... No lives LEFT!!!👿")
                }
            }
        }

    }

    override fun toString(): String = "(|    |ENEMY INFO|    |)\n" +
                                      "---------------------------\n" +
                                      "Name: [ $_name ]\n" +
                                      "Hit Points: [ $_hitPts ]\n" +
                                      "Lives: [ $_lives ]\n" +
                                      "---------------------------\n"
}