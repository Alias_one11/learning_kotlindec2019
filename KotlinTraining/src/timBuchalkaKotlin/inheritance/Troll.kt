package timBuchalkaKotlin.inheritance

/** [Inheriting] from the [Enemy] class. Since the
 *   [Troll] class has default values for hitPts & lives,
 *   we just need to provide a name to the troll when creating
 *   an instance.*/
class Troll(_name: String) : Enemy(_name, 27, 1)