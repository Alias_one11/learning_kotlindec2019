package timBuchalkaKotlin.inheritance

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.*

fun main()
{
    //==============================================
    "Inheritance".codeDiv()
    //==============================================
    "Testing the enemy instance 'toString()' method".printHeader()

    val enemy = Enemy("Num-z The-Evil", 10, 3)
    println(enemy)

    "Testing the 'takeDmg method works'".section()
    enemy.takeDmg(4)
    println(enemy)

    "Seeing if the enemy loses lives".section()
    enemy.takeDmg(11)
    println(enemy)

    "Troll inherits from Enemy".section()
    val troll = Troll("Ugly-Troll")
    println(troll)

    "The troll taking Damage & Dieing".section()
    troll.takeDmg(17)
    // Troll has 10 damage points
    // left before it dies
    println(troll)
    // Let's kill the troll!!!
    troll.takeDmg(10)
    println(troll)
    //==============================================

    "Creating a Vampire instance".printHeader()
    val vampiro = Vampire("Dracula's Little Brother")
    "New Vampiro".section()
    println(vampiro)

    /** Testing the overriding method [takeDmg] &
     *    seeing if it only takes half the damage from
     *    the vampiro instance.*/
    vampiro.takeDmg(8)
    println(vampiro)
    //==============================================

    "Vampire Battle".codeDiv()

    /** [takeDmg] was overriding twice. Once in the [Vampire]
     *  class & again in the [VampireKing] class. Both methods
     *  divide the [takeDmg] by 2. So the [Vampire] damage of
     *  12 will be divided by 2 twice. The [vampiroBattle]
     *  function starts a battle & ends it before the
     *  [ElReyVampiro] runs out of lives. The function also
     *  creates a new instance of [VampireKing], which you
     *  then pass the name parameter to the function argument */
    vampiroBattle("El-Rey-Vampiro")

}

fun vampiroBattle(name: String) {
    // Creating a vampire instance
    VampireKing(name)
        .apply {
            println(this)
            // Lets damage El-Rey-Vampiro
            // repeatedly with a while loop
            quickExit(this)
            println(this)
    }
}
