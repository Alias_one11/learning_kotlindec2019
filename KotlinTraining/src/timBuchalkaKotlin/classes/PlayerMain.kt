package timBuchalkaKotlin.classes

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.*

fun main()
{
    //==============================================
    "Classes".codeDiv()
    //==============================================
    "Example#1".printHeader()

    "Two Arg Constructor".section()
    val alias111 = Player("Alias111", 5)
    alias111.toString()

    "Three Arg Constructor".section()
    val sami = Player("Sami", 4, 8)
    sami.toString()

    "All Arg Constructor".section()
    val alisia = Player("Alisia", 2, 5, 1000)
    alisia.toString()

    "Using-Properties Of Objects".section()

    val jay = Player("Jay", 2, 5, 1000)
    jay.toString()
    println("Weapon Name: [ ${jay.weaponEquip()} ]")
    println("Damage From Attack: [ ${jay.weapon.dmgInficted} ]")

    //=================================================================
    "New Weapon For Player".section()

    val lanister = Player("Lanister", 2, 5, 1000)

    // Creating a new weapon
    val axe = Weapon("Axe", 12)
    lanister.weapon = axe
    // Equipped the new weapon
    lanister.weaponEquip()
    // Damage effect
    val dmg =  axe.dmgInficted

    lanister.toString()
    println("> Damage Effect:=> [ $dmg ]")
}

fun Player.weaponEquip() {
    val result = weapon.name.toUpperCase()
    println("> Weapon Equipped: [$result]\n")
}
