package timBuchalkaKotlin.classes

import java.text.NumberFormat
import java.text.NumberFormat.getCurrencyInstance



class Loot(internal val name: String, internal val type: LootType, val value: Double) {

    private val formatter: NumberFormat by lazy { getCurrencyInstance() }

    override fun toString(): String {
        val formatVal = formatter.format(value)

        return "<[$name] is a type\n[$type] & is worth [$formatVal]>"
    }
}

enum class LootType {
    POTION,
    RING,
    ARMOR
}