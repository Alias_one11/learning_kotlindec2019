package timBuchalkaKotlin.classes

class Player(
        private var name: String,
        private var level: Int = 5,
        private var lives: Int = 3,
        private var score: Int = 0) {

    /** [PROPERTY] */
    var weapon: Weapon = Weapon("Fist", 1)

    /** [LIST] [internal] If you are using the internal modifier in the
     *  declaration then it will be visible everywhere in that
     *  particular module. By the term module, we mean to say
     *  those files that are compiled together. The internal
     *  modifier is beneficial only when we are
     *  having more than one modules in a project.*/
    internal val inventory = arrayListOf<Loot>()

    // Prints out the players information
    fun playerInfo() {
        when {
            lives > 0 -> println("$name is alive")
            else -> println("$name is dead")
        }
    }

    override fun toString(): String {
        return """
<<<< Player-Info >>>>            
Name: [ $name ]
Lives: [ $lives ]
Level: [ $level ]
Score: [ $score ]
Weapon: [ $weapon ]
        """.trimIndent()
                .apply { println(this) }
    }

    fun showInventory() {
        println("<<( |[ $name ] Loot-Inventory:| )>>")
        println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        for (item in inventory) {
            println("-------------------------------")
            println(item)
            println("-------------------------------")
        }
        println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    }

    // Removes loot from the players inventory
    fun dropLoot(item: Loot):Boolean {
        return when (item) {
            in inventory -> {
                inventory.removeIf { it.name == name }
                true
            }
            else -> false
        }
    }
}