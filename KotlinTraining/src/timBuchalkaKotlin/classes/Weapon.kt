package timBuchalkaKotlin.classes

class Weapon(val name: String, var dmgInficted: Int) {

    override fun toString(): String {
        return "[$name] inflicts [$dmgInficted] points of damage"
    }
}