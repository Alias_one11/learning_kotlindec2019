package generics

import kt_essentials.programflow.functions.formatMsgAnswerStr

open class WaterSupply(var needsProcessing: Boolean)

/** [subclass]: */
class TapWater : WaterSupply(needsProcessing = true) {

    fun addChemicalCleaners() {
        formatMsgAnswerStr(msg = "Added [CHEMICAL CLEANER]",
                           answer = "Status::[CLEAN]")

        needsProcessing = false
    }
}