package generics

import kt_essentials.programflow.functions.formatMsgAnswerStr

class FishStoreWater : WaterSupply(needsProcessing = false)

class LakeWater : WaterSupply(needsProcessing = true) {

    fun filter(): Boolean {
        formatMsgAnswerStr(msg = "Filtering AQUARIUM [PROCESSING...]",
                           answer = "Status::[BACTERIA-GONE]-->Clean..")

        needsProcessing = false
        return needsProcessing
    }
}