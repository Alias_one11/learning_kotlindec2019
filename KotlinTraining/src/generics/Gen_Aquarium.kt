package generics

import kt_essentials.programflow.functions.formatMsgAnswerStr

/****************************************************************
 * by default, [T] stands for the [nullable] [Any]? type, the
 * type at the top of the type hierarchy. So since its [nullable],
 * it cannot be [instantiated] with Object(null)
 *---------------------------------------------------------------
 * To not allow passing [null], make [T] of type [Any] explicitly,
 * by removing the [?] after [Any].
 * --------------------------------------------------------------
 * In this context, [Any] is called a [generic] constraint. It means
 * any type can be passed for [T] as long as it isn't [null].
 * --------------------------------------------------------------
 * [val] and [var] are about the [VALUES] of variables. [val]
 * [protects] the variable value from being changed. [in] and
 * [out] are about the [TYPES] of variables. [in] and [out] make
 * sure that when working with [generic] types, only [safe]
 * types are passed [in] and [out] of [functions].
 * --------------------------------------------------------------
 * If you remove the [out] keyword, the [compiler] will give an error
 * when calling addItemTo(), because Kotlin can't ensure that you
 * are not doing anything unsafe with the type.
 ****************************************************************/
class GenAquarium<out T: WaterSupply>(val waterSupply: T) {

    fun addWater(cleaner: Cleaner<T>) {

        when { waterSupply.needsProcessing -> cleaner.clean(waterSupply) }
        formatMsgAnswerStr(msg = "Adding water from",
                           answer = waterSupply to "[Lake-Water]")

        /******************************************************
             * [Throws] an [IllegalStateException] with the result
             *  of calling [lazyMessage], if the value is [false].
             *  In this case, if [needsProcessing] is [true],
             *  check() will [throw] an [exception].
             ******************************************************/
            //        check(waterSupply.needsProcessing.not(), fun(): String {
            //            return "[[water supply needs processing first]]"
            //        })
            //        formatMsgAnswerStr(msg = "Adding water from",
            //                           answer = waterSupply to "[Lake-Water]")
        /******************************************************
         * [Throws] an [IllegalStateException] with the result
         *  of calling [lazyMessage], if the value is [false].
         *  In this case, if [needsProcessing] is [true],
         *  check() will [throw] an [exception].
         ******************************************************/
//        check(waterSupply.needsProcessing.not(), fun(): String {
//            return "[[water supply needs processing first]]"
//        })
//        formatMsgAnswerStr(msg = "Adding water from",
//                           answer = waterSupply to "[Lake-Water]")
    }

    /**********************************************************
     * To do an is check, you need to tell [Kotlin] that the
     * type is [reified], or [real], and can be used in the
     * function. To do that, put [inline] in front of the [fun]
     * [keyword], and [reified] [in] in front of the [generic]
     * type [R].
     * --------------------------------------------------------
     * [Note]: [Generic] types are normally only available at
     * [compile] [time], and get replaced with the [actual]
     * [types]. To keep a [generic] type available until [run]
     * [time], declare the function [inline] and make the type
     * [reified]. Once a type is reified, you can use it like
     * a normal type—because it is a real type after inlining.
     * That means you can do is checks using the type.
     **********************************************************/
    inline fun <reified R: WaterSupply> hasWaterSupplyOfType() = waterSupply is R
}

/****************************************************************
 * The [in] type is similar to the [out] type, but
 * for [generic] types that are only {ever passed into functions},
 * not [returned]. If you try to [return] an [in] type, you'll get
 * a [compiler] [error].
 ****************************************************************/
interface Cleaner<in T: WaterSupply> {
    fun clean(waterSupply: T)
}

fun <T: WaterSupply> isWaterClean(aquarium: GenAquarium<T>) {
    aquarium.waterSupply.needsProcessing.also { processing: Boolean ->
        formatMsgAnswerStr("aquarium water is clean", processing)
    }
}

typealias AquaPark = GenAquarium<WaterSupply>
fun addItemTo(aquarium: AquaPark): String{
    return "[[ITEM-ADDED==]]\n${aquarium to "[New-Water]"}"
        .also(::println)
}