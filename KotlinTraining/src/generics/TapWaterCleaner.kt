package generics

typealias CleanTapH2O = Cleaner<TapWater>
class TapWaterCleaner : CleanTapH2O {
    override fun clean(waterSupply: TapWater) = waterSupply.addChemicalCleaners()
}
