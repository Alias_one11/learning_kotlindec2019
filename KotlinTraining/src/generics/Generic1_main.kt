package generics

import kt_essentials.programflow.functions.*

fun genericsExample() {
    val aquarium: GenAquarium<TapWater> = GenAquarium(TapWater())

    /** Before cleaning the water*/
    formatMsgAnswerStr(msg = "water needs processing..[DIRTY]",
                       answer = aquarium.waterSupply.needsProcessing)
    aquarium.waterSupply.addChemicalCleaners()
    /** After cleaning the water*/
    formatMsgAnswerStr(msg = "[WATER-PROCESSED]...Dirty?",
                       answer = aquarium.waterSupply.needsProcessing)
}

//fun genericsExample2() {
//    val aquarium: GenAquarium<LakeWater> = GenAquarium(LakeWater())
//    aquarium.waterSupply.filter()
//    aquarium.addWater()
//    addItemTo(aquarium)
//}

fun genericsExample3() {
    val cleaner = TapWaterCleaner()
    val aquarium: GenAquarium<TapWater> = GenAquarium(TapWater())
    aquarium.addWater(cleaner)
    isWaterClean(aquarium)
}

fun genericsExample4(): Boolean {
    val aquaPark: GenAquarium<WaterSupply> = AquaPark(TapWater())

    return aquaPark.hasWaterSupplyOfType<TapWater>()
        .also { active: Boolean ->
        formatMsgAnswerStr("aquarium tap water is clean", active)
    }
}



fun main()
{
    //===========================================
    "Generics-Part#1".printHeader()
    "".section()
    //===========================================

    genericsExample()
//  genericsExample2()
    genericsExample3()
    genericsExample4()

}