package kotlinpatterns.factorypattern

import kotlinpatterns.factorypattern.ICar.Brand.*
import kt_essentials.programflow.functions.*

fun main()
{
    //===========================================
    "Factory-Pattern".printHeader()
    "TESTING OUR FACTORY Method".section()
    //===========================================

    val customer1 = CarFactory()
    val customer2 = CarFactory()
    val customer3 = CarFactory()
    val customer4 = CarFactory()
    val customer5 = CarFactory()
    val customer6 = CarFactory()
    //===========================================

    /** Let us rent some [cars] 😎*/
    customer1.rentCar(BMW)
    customer2.rentCar(AUDI)
    customer3.rentCar(MERCEDES)
    customer4.rentCar(JAGUAR)
    customer5.rentCar(RANGEROVER)
    customer6.rentCar(PORSCHE)
    //===========================================


}