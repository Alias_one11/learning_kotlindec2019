package kotlinpatterns.factorypattern

import kotlinpatterns.factorypattern.ICar.Brand
import kotlinpatterns.factorypattern.ICar.Brand.*
import kt_essentials.programflow.functions.formatMsgAnswerStr

/********************************
 *     BMW, AUDI,
 *     MERCEDES, JAGUAR,
 *     RANGEROVER, PORSCHE
 ********************************/

open class CarFactory {

    open fun rentCar(brand: Brand):ICar? = brand.whenCarEquals

    private val Brand.whenCarEquals: ICar?
        get() {
            return when (this) {
                BMW -> {
                    formatMsgAnswerStr(msg = "Customer has rented", answer = "Bmw")
                    Bmw()
                }
                AUDI -> {
                    formatMsgAnswerStr(msg = "Customer has rented", answer = "Audi")
                    Audi()
                }
                MERCEDES -> {
                    formatMsgAnswerStr(msg = "Customer has rented", answer = "Mercedes")
                    Mercedes()
                }
                JAGUAR -> {
                    formatMsgAnswerStr(msg = "Customer has rented", answer = "Jaguar")
                    Jaguar()
                }
                RANGEROVER -> {
                    formatMsgAnswerStr(msg = "Customer has rented", answer = "RangeRover")
                    RangeRover()
                }
                PORSCHE -> {
                    formatMsgAnswerStr(msg = "Customer has rented", answer = "Porsche")
                    Porsche()
                }
            }
        }
}