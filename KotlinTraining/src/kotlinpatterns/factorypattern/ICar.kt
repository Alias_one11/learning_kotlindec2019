package kotlinpatterns.factorypattern

interface ICar {

    enum class Brand {
        BMW, AUDI, MERCEDES,
        JAGUAR, RANGEROVER, PORSCHE
    }
}
/****************************
 * Classes extending [ICar]*/
class Bmw: ICar
class Audi: ICar
class Mercedes: ICar
class Jaguar: ICar
class RangeRover: ICar
class Porsche: ICar
/***************************/
