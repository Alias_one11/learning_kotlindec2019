package abstract_factorypattern

import abstract_factorypattern.EnumInfantryUnits.*
import abstract_factorypattern.EnumVehicleUnit.*
import kt_essentials.programflow.functions.*

fun main()
{
    //===========================================
    "Abstract-Factory-Pattern".printHeader()
    "TESTING OUR Abstract-Factory Method".section()
    //===========================================
    val hq = HeadQuarterHQ()
    val barracks1 = Barracks()
    val barracks2 = Barracks()
    val vehicleFactory1: VehicleFactory = hq.buildVehicleFactory()

    listOf(
        barracks1.build(RIFLEMAN),
        barracks2.build(ROCKET_SOLDIERS),
        vehicleFactory1.build(ARMORED_HUMVEE),
        vehicleFactory1.build(M2_TANK)
    )





}