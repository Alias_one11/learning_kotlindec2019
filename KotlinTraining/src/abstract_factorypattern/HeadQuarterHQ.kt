package abstract_factorypattern

class HeadQuarterHQ {
    private val bldg = mutableListOf<IBuilding<*, IUnit>>()

    fun buildBarracks(): Barracks {
        val barracks = Barracks()

        bldg.add(barracks)
        return barracks
    }

    fun buildVehicleFactory(): VehicleFactory {
        val vehicleFactory = VehicleFactory()

        bldg.add(vehicleFactory)
        return vehicleFactory
    }
}