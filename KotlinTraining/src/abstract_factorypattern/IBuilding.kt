package abstract_factorypattern

interface IBuilding<in UnitType, out ProducedUnit>
        where UnitType: Enum<*>, ProducedUnit: IUnit {

    fun build(type: UnitType): ProducedUnit
}