package abstract_factorypattern

enum class EnumVehicleUnit {
    ARMORED_HUMVEE,
    M2_TANK
}

/** interface [IVehicle]*/
interface IVehicle: IUnit

/** classes [ArmoredHumvee] [M2Tank]*/
class ArmoredHumvee: IVehicle
class M2Tank: IVehicle
