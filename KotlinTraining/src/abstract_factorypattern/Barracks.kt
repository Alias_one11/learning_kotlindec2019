package abstract_factorypattern

import abstract_factorypattern.EnumInfantryUnits.*
import kt_essentials.programflow.functions.formatMsgAnswerStr

class Barracks: IBuilding<EnumInfantryUnits, Iinfantry> {

    override fun build(type: EnumInfantryUnits)
            : Iinfantry = when(type) {

        RIFLEMAN -> {
            formatMsgAnswerStr(
                msg = "Orders From The HEADQUARTERS",
                answer = "$type::Activated"
            )
            Rifleman()
        }
        ROCKET_SOLDIERS -> {
            formatMsgAnswerStr(
                msg = "Orders From The HEADQUARTERS",
                answer = "$type::Activated"
            )
            RocketSoldier()
        }
    }
}