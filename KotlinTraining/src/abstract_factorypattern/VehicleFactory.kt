package abstract_factorypattern

import abstract_factorypattern.EnumVehicleUnit.*
import kt_essentials.programflow.functions.formatMsgAnswerStr

class VehicleFactory: IBuilding<EnumVehicleUnit, IVehicle>{

    override fun build(type: EnumVehicleUnit): IVehicle = when(type) {

        ARMORED_HUMVEE -> {
            formatMsgAnswerStr(
                msg = "Orders From The HEADQUARTERS",
                answer = "$type::Activated"
            )
            ArmoredHumvee()
        }
        M2_TANK -> {
            formatMsgAnswerStr(
                msg = "Orders From The HEADQUARTERS",
                answer = "$type::Activated"
            )
            M2Tank()
        }

    }


}