package abstract_factorypattern

enum class EnumInfantryUnits {
    RIFLEMAN,
    ROCKET_SOLDIERS
}

/** interfaces [IUnit] [Iinfantry]*/
interface IUnit
interface Iinfantry: IUnit

/** clases [Rifleman] [RocketSoldier]*/
class Rifleman: Iinfantry
class RocketSoldier: Iinfantry