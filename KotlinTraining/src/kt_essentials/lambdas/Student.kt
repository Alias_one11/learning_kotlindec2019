package kt_essentials.lambdas

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

data class Student(val name: String, var age: Int)

fun getStudent(): List<Student> {
    return listOf(
        Student("Alice", 19),
        Student("Sami", 23),
        Student("Bobby", 22),
        Student("Don", 19),
        Student("Tim", 18)
    )
}

fun main()
{
    //==============================================
    "Lambdas".codeDiv()
    //==============================================
    "Testing Our Lambdas".printHeader()

    // Getting our list of students
    val students: List<Student> = getStudent()

    // Using lambdas to combine
    // there name & age together
    combineInfo(students)

    byAge(students)

    longestName(students)
}



