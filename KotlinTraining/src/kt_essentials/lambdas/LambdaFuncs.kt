package kt_essentials.lambdas

fun combineInfo(students: List<Student>) {
    val combo: List<String> = students
        .map { a: Student ->
            "Student Info:--> ${a.name} : ${a.age}\n"
        }
    println(combo)
}

fun longestName(students: List<Student>) {
    /** Instead of using the [it] keyword we created
     *  our own explicitly & named it [the] to use
     *  as [the: Student -> the.name]*/
    val studentsLongNames: List<Student> = students
        .filter { the: Student -> the.name.length > 4 }
    println("\nList by length of name:\n $studentsLongNames\n")
}


fun byAge(students: List<Student>) {
    /** Same thing as a [lambda] but by [reference] instead */
    // println("The oldest student: ${students.maxBy(Student::age)}")

    /** [it] is used to access parameter values in
     *  the body of the lambda expression.*/
    println("\nThe oldest  student: ${students.maxBy { it.age }}\n")
}
