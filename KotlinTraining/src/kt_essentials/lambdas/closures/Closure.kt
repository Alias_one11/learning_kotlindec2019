package kt_essentials.lambdas.closures

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

/** [Closures] are functions that can access
 *  and modify properties defined outside the
 *  scope of the function.*/
fun closureMaker(name: String): () -> Unit {
    var num = 0
    return {
        println("Calls to the closure $name: [${num++}]")
    }
}

fun main()
{
    //==============================================
    "Closures".codeDiv()
    //==============================================
    "Testing Our Closures".printHeader()

    val counter1: () -> Unit = closureMaker("counter1")
    val counter2: () -> Unit = closureMaker("counter2")

    counter1()
    counter1()
    counter1()
    counter1()
    counter2()
    counter2()
    counter1()
}