package kt_essentials.lambdas.methodchaining

import kt_essentials.lambdas.*

/** <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 *  [Maps] over the array, [squares] it & then extracts
 *  the [even] Int. The method [rem]:==> Calculates the
 *  remainder of dividing this value by the other value.
 *  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
fun evenNum() {

    val squares: Sequence<Int> = generateSequence(1) {
        return@generateSequence it.plus(1)
    }.map { return@map it.times(it) }

    val evenSquares: Sequence<Int> = squares.filter { it.rem(2) == 0 }
    val evenIt: List<Int> = evenSquares.take(5).toList()

    println("(|Even Squares|):\n${evenIt}\n")
}

/** <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 *  One way to create a [sequence] is to convert a [collection]
 *  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
fun seqStudents() {
    val students: List<Student> = getStudent()

    val seqStudents: List<Student> = students.drop(3).take(2).toList()

    println("(|Sequenced Students|):\n$seqStudents\n")
}

/**<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 * Generating [sequences] on the fly, with [lambdas]
 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
fun lambdaSeq() {
    val numbers: Sequence<Int> = generateSequence(50) { it.plus(1) }
    val rearrange: List<Int> = numbers.drop(5).take(10).toList()

    println("(|Numbers|):\n$rearrange\n")
    /** will [return]: 2 * 2 = 4; 4 * 4 = 16..etc
     *  the function will take in 10 integers but drop
     *  5 & output five numbers squared starting from 1*/
}


