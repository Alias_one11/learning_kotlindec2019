package kt_essentials.lambdas.methodchaining

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun sequences() {
    seqStudents()
    lambdaSeq()
    evenNum()

    /**<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
     * Generating [sequences] of fibonacci numbers,
     * set to a pair of 1 to 1.
     * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    val fib: Sequence<Int> = generateSequence(seed = 1 to 1)
    { it: Pair<Int, Int> ->
        return@generateSequence it.second to it.first.plus(it.second)
    }.map(Pair<Int, Int>::first)// Reference instead of "it" call

    val fibSeq: List<Int> = fib.take(15).toList()

    println("(|Fibonacci number sequence|):\n$fibSeq")
}

fun main()
{
    //==============================================
    "Sequences".codeDiv()
    //==============================================
    "Testing Our Sequences".printHeader()

    /** There will be one dropped */
    sequences()
}

