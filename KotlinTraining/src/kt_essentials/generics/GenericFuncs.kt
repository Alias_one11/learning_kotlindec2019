package kt_essentials.generics

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 *  [Generic]: Function
 *  Since [Kotlin] provides the programmer, to define new types
 *  in terms of classes, there must be a way to compare the
 *  instances of these classes. As in [Java], the [Comparable]
 *  interface provides a [compareTo_function] to compare two
 *  objects. [Kotlin] provides this through the [Comparable]
 *  interface. However, it also provides certain extension
 *  functions, which provides much more functionality. [Kotlin]
 *  also provides an additional advantage that the instance
 *  implementing [Comparable_interface] can be compared using
 *  relational operators.
 *  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

fun < T: Comparable<T> > genericMax(aParam1: T, aParam2: T): T {
    /** [compareTo_function] –
    This function compares the calling object with the
    passed object. It returns zero, if both are equal,
    a negative number if the passed object is greater,
    otherwise it returns a positive number.
    */
    val results = aParam1.compareTo(aParam2)
    return when {
        (results > 0) -> aParam1
        else -> aParam2
    }
}

@JvmOverloads fun <T> printFormat(
    aHeaderMsg: String,
    msgVar1: String = "",
    msgVar2: String = "",
    msgVar3: String = "",
    msgVar4: String = "",
    generic1: T? = null,
    generic2: T? = null,
    generic3: T? = null,
    generic4: T? = null): Unit = println(
    "=======(|$aHeaderMsg|)=======\n" +
    "[(|$msgVar1|)]:\n>[$generic1]\n" +
    "\n[|($msgVar2)|]|:\n>[$generic2]\n" +
    "\n[|($msgVar3)|]:\n>[$generic3]\n" +
    "\n[(|$msgVar4|)]:\n>[$generic4]\n"
)