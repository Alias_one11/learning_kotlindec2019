package kt_essentials.generics

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Generics-In-Kotlin".codeDiv()
    //==============================================
    "Testing Our Generics".printHeader()

    val maxInt: Int = genericMax(42, 99)
    val maxLong: Long = genericMax(123456789L, 333333333L)
    val maxByte: Byte = genericMax((-128).toByte(), (127).toByte())
    val maxStr: String = genericMax("Alpha", "Beta")

    printFormat(
        aHeaderMsg = "GENERIC_FUNCTION",
        msgVar1 = "maxInt", msgVar2 = "maxLong",
        msgVar3 = "maxByte", msgVar4 = "maxStr",
        generic1 = maxInt, generic2 = maxLong,
        generic3 = maxByte, generic4 = maxStr
    )
}
