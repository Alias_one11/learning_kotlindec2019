package kt_essentials.programflow

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Iterate-Through An Array".codeDiv()

    val colors: Array<String> = arrayOf("Red", "Green", "Blue")
    val values: IntArray = intArrayOf(1, 3, 5, 7, 9)

    "For-each-loop".printHeader()
    for (color in colors) {
        println(color)
    }

    for (value in values) {
        println(value)
    }

    "For-each-loop With-Indices".printHeader()
    for (i in values.indices) {
        println("Getting each value by its index\n" +
                "in the array: ${values[i]}")
    }

"For-each-loop With-Indices step to index by 2".printHeader()
    for (i in values.indices step 2) {
        println(
                "Getting each value by its index\n" +
                "in the array stepping by\n" +
                "every other property: ${values[i]}"
        )
    }

    "For-each-loop checking size".printHeader()
    for (i in 0 until colors.size) {
        println(colors[i])
    }

    //==============================================
    "Extra-lesson: while & do-while".printHeader()

    val states = arrayOf("CA", "OR", "WA")

    "While-loop".codeDiv()
    var counter = 0
    while (counter < states.size) {
        println("Counter = [ $counter ]  " +
                "State = [ ${states.get(index = counter)} ]")
        counter++
    }

    "Do-while".codeDiv()

    counter = 0
    do {
        println("State = [ ${states.get(index = counter)} ]")
        counter++
    } while (counter < states.size)
}


