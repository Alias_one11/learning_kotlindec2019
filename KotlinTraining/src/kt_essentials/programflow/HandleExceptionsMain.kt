package kt_essentials.programflow

import kt_essentials.basics.codeDiv
import java.lang.NumberFormatException

fun main()
{
    //==============================================
    "Handling-Exceptions".codeDiv()

    try {
        print("Value 1: ")
        val value1: String? = readLine()
        val chgToDouble:Double = value1!!.toDouble()

        print("Value 2: ")
        val value2: String? = readLine()
        val chgToDouble2:Double = value2!!.toDouble()

        val sum = chgToDouble.plus(chgToDouble2)
        println("Sum = [ $sum ]")

    } catch (e: KotlinNullPointerException) {
        println("[ERROR:=> VALUE WAS NULL]")

        /** NumberFormatException is a JVM
         *  universally used exception:=>
         *  Java, Scala..Etc */
    } catch (e: NumberFormatException) {
        /** "message" is a property in
         *  the exception class in kotlin.
         *  In this case, message will represent
         *  what ever the user input is, & will
         *  be added to the user exception message.*/
        println("${e.message} is not a number.")
    }
    /** "Throwable" is the kotlin exception class. The
     *  base class for all errors and exceptions.Only
     *  instances of this class can be thrown or caught.
        @param "message" the detail message string.
        @param "cause" the cause of this throwable. */
}