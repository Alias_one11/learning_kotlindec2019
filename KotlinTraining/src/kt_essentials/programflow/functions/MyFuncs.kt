package kt_essentials.programflow.functions

import kt_essentials.create_custom_classes.dataclasses.ClothingItem
import timBuchalkaKotlin.classes.*
import java.text.NumberFormat

// Addition function
fun Double.addVal(param2: Double): Double = this + param2

// Does all types of calculations in one function
fun Double.calcVal(param2: Double, operator: String = "+"): Double {

    when (operator) {
        ("+") -> return this + param2
        ("-") -> return this - param2
        ("*") -> return this * param2
        ("/") -> return this / param2
        ("%") -> return this % param2
    }
    return  -1.0
}

fun Double.changeSomething() {
    var copy: Double = this
    copy++
    println("Copy++ is:=> [ $copy ]")
}

/** NOTE!!! the built in kotlin function
 *  .sum() does the same thing. */
// Adds all values in an array
fun Array<Int>.addVals(): Int {
    var result = 0

    for (i in this) result += i
    return result
}

fun String.printHeader() {
    print("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    println("\n                  $this                  ")
    println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")


}

/** [CarFactory] : formatter function */
fun <T> formatMsgAnswerStr(msg: String, answer: T) {
    println("|-----------------------------------------------")
    println("|(| $msg |):")
    println("|[ $answer ]")
    println("|-----------------------------------------------")
}

fun checkOut(shoppingCart: MutableMap<ClothingItem, Int>) {
    /** Looping through our cart and then outputting the total */
    var total = 0.0
    // Locale.setDefault(Locale.GERMANY)
    val formatter = NumberFormat.getCurrencyInstance()
    for ((item, quantity) in shoppingCart) {
        val itemTotal = quantity.times(item.price)

        println("Item: [ ${item.type} ]\n" + "Price: [ ${item.price} ]\n" + "Cart Total: [ $itemTotal ]")
        total = total.plus(itemTotal)
    }

    println("------------------------------------")
    println("Total: [ ${formatter.format(total)} ]")
}

fun String.section() {
    println("=================================================================")
    println("(|${this}|)\n")
}

fun printArr(arr: Array<Int>?):Unit? = arr?.forEach { i -> println(i) }


fun Player.dumped(item: Loot):Unit = when {
    // Calls the function from the player class
    // to remove an item from the inventory
    dropLoot(item) -> {
        println("[<${item.type} has been dropped>]\n")
        showInventory()
    }
    else -> println(
                "::(|Is the ${item.name} in the inventory?|)::\n" +
                "|(you do not have a [${item.name} ])|\n"
                   )
}