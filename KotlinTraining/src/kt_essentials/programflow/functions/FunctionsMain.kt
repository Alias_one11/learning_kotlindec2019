package kt_essentials.programflow.functions

import kt_essentials.basics.*

fun main()
{
    //==============================================
    "Kotlin-Functions".codeDiv()

    val num1 = 4.0
    val num2 = 3.5

    // Using the first parameter as a receiver
    // then adding them "this + param2"
    val result = num1.addVal(param2 = num2)
    println("<|func addVal()|>\nnum1 + num2 = [ $result ]\n")

    //***********************************************************
    /** The set of functions below explained:The second argument
     *  is the operator being used for the function, but it has
     *  a default call of addition "+" if nothing is called as a
     *  second argument to the function. The first argument is
     *  the receiver, which allows to call the function by dot
     *  notation, vs passing all arguments inside the parenthesis
     *  of the function once it is called. */
    //***********************************************************

    /* Add */
    val adding = num1.calcVal(num2)// "+"
    println("<|func calcVal(num2)|>\nnum1 + num2 = [ $adding ]\n")
    /* Subtract */
    val subtracting = num1.calcVal(num2, "-")// "+"
    println("<|func calcVal(num2)|>\nnum1 - num2 = [ $subtracting ]\n")
    /* Multiply */
    val multi = num1.calcVal(num2, "*")// "+"
    println("<|func calcVal(num2)|>\nnum1 * num2 = [ $multi ]\n")
    /* Divide */
    val divide = num1.calcVal(num2, "/")// "+"
    println("<|func calcVal(num2)|>\nnum1 / num2 = [ ${divide.roundIt()} ]\n")
    /* Modular */
    val modular = num1.calcVal(num2, "%")// "+"
    println("<|func calcVal(num2)|>\nnum1 % num2 = [ $modular ]\n")

    // Incrementing by using a function that changes the state
    val changeMe = 16.0
    changeMe.changeSomething()

    // Adds up all the values in an array
    val numArray:Array<Int> = arrayOf(1, 2, 3, 4, 5, 6)
    val arrayResult = numArray.addVals()// Basically .sum()
    println(
            "\n<|Result of adding all\n" +
            "the values in the array|>: " +
            "[ $arrayResult ]"
    )
}


