package kt_essentials.programflow

fun main()
{
    // could be a null value
    val nullVal: String? = "I'm a String"
    println("This values is:=> [ $nullVal? ]")

    // The two question marks ??
    // makes the unwrapping safe
    val ternary: Int? = nullVal?.length
    println("The unwrap is safe. Length is:=> [ $ternary ]")

    // Using the Elvis operator
    val elvis: Int = ternary ?: -1
    println("Is the value null?:=> [ $elvis ]")

    // Null assertions. To see if a variable
    // really isn't null, you can trigger
    // it with a double exclamation marks. "!!"
    // or force unwrapping
    try {
        val assert = ternary!!
        println("Is the variable null?: [ $assert ]")
    } catch (e: KotlinNullPointerException) {
        println("ternary is null!!!")
    }

}