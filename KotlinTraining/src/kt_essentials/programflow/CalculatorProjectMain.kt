package kt_essentials.programflow

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.calcVal

fun main()
{
    //==============================================
    "Calculator".codeDiv()
    //==============================================
    try {
        print("Value #1: ")
        val str1: String? = readLine()
        val num1 = str1 !!.toDouble()

        print("Value #2: ")
        val str2: String? = readLine()
        val num2 = str2 !!.toDouble()

        print("Select an operator:=>\n" +
                " [?=> |'+'| |'-'| |'/'| |'*'| |'%'| <=?]")

        val result: Double? = when (val operation: String = readLine() !!) {
            "+" -> num1.calcVal(num2, operation)
            "-" -> num1.calcVal(num2, operation)
            "/" -> num1.calcVal(num2, operation)
            "*" -> num1.calcVal(num2, operation)
            "%" -> num1.calcVal(num2, operation)
            else -> throw Exception("[ERROR:=> UNKNOWN OPERATION]")
        }

        /** THE RESULT OF THE CALCULATION */
        println("The answer is:=> [ $result ]")

    } catch (e: NumberFormatException) {
        val msg: String? = e.message
        println("[ $msg IS NOT A NUMBER]")

    } catch (e: Exception) {
        val msg: String? = e.message
        println(msg)
    }
}