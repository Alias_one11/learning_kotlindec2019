package kt_essentials.programflow

import kt_essentials.basics.codeDiv

fun main()
{
    //==============================================
    "UserInput-EvaluateCondition".codeDiv()

    println("[EXAMPLE #1]")

    println("<|Enter a state abbreviation|>")
    /** Unlike in java 🤮 readLine is a top-level
     *  function. So its always available.*/
    val state: String? = readLine()

    /** So the input does not have to distinguish
     *  from lower case or upper case. */
    val stateAnyType: String? = state?.toUpperCase()

    val capital: String?
    capital = when (stateAnyType) {
        "CA" -> "Sacramento"
        "OR" -> "Salem"
        "MA" -> "Boston"
        "NY" -> "Albany"
        "CO" -> "Denver"
        "FLO" -> "Tallahassee"
        "GA" -> "Atlanta"
        "ME" -> "Augusta"
        else -> "Unknown"
    }
    println("The capital is [ $capital ]")

    //==============================================
    println("\n[EXAMPLE #2]")

    when (stateAnyType) {
        "CA", "OR", "WA" -> println("The:=> West-Coast")
        "NH", "VT", "MA" -> println("_:=> New-England")
        "TX", "FL", "GA" -> println("The:=> Dirty-South")
        else -> println("Somewhere else")
    }

    when (50) {
        in 1..39 -> println("\nNope. Try again...")
        in 40..45 -> println("\nNope. Try again...")
        else -> {
            println("\n[If your down here your hella lost!!!]")
            println("[I can print as much as i want down here!]")
        }
    }
}