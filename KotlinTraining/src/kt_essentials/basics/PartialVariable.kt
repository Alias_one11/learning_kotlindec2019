package kt_essentials.basics

import java.math.RoundingMode
import kotlin.math.roundToLong

class Constant {
    companion object {
        /** The constant will be available throughout
            your application & is public by default
            The companion objects are good to store
            what would be considered static functions
            or in this case const variables that would
            be static in java. It also does everything
            at compile time vs runtime */
        const val RED = "Red"
        @JvmField val BLUE = "Blue"
    }
}

/** A function that takes a string argument
 *  of the section im in, and prints out the
 *  string spacing out the console output better  */
fun String.codeDiv() {
    println("==================================================================")
    println("                            $this                                 ")
    print("==================================================================\n")

}

fun Double.rdNearPlace(): Double {
    return toBigDecimal()
            .setScale(1, RoundingMode.DOWN)
            .toDouble()
}

// Rounds to the nearest 0.01 place
fun Double.roundIt(): Double {
    return (this * 100.0).roundToLong() / 100.0
}