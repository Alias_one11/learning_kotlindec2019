package kt_essentials.basics

import kt_essentials.basics.Constant.Companion.BLUE
import kt_essentials.basics.Constant.Companion.RED
import kotlin.Boolean.Companion.equals

fun main()
{
    "Numbers-NumberMethods".codeDiv()

    var num1 = 5
    println("The initial value of num1: [ $num1 ]")
    num1++
    println("The new value of num1: [ $num1 ]")

    val num2: Double = 5.toDouble()
    println("The value of num2: [ $num2]\n")

    val myName = "Alias111"
    println("My name: [ $myName ]\n")

    println("------------------------------")

    val n1 = 10
    val n2 = 10

    val match = (n1 == n2)
    println("Is n1 == to n2: [ $match ]")

    // Comparing using a method
    val n3 = 13
    val n4 = 13

    val matchWithMethod: Boolean = equals((n3.compareTo(n4)))

    if (matchWithMethod) {
        println("Does n3 == n4: [ true ]")
    } else println("n3 != n4: [ false ]")

    println("------------------------------")

    //************* plus() ******************
    val uno = 10
    val dos = 7

    val laSuma = uno + dos
    println("uno + dos = [ $laSuma ]")

    // Now lets use a method that does the same thing
    val uno2 = 10
    val dos2 = 7

    val laSuma2 = uno2.plus(dos2)
    println("uno2.plus(dos2) = [ $laSuma2 ]\n")

    //************* minus() ******************
    val y = 10
    val x = 3

    val laSuma3 = y - x
    println("y - x = [ $laSuma3 ]")

    val y2 = 10
    val x2 = 3

    val s = y2.minus(x2)
    println("y2.minus(x2) = [ $s ]\n")

    //************* inc() ******************
    val u = 3

    // The same as u++
    val weUpOne = u.inc()
    println("u.inc() = [ $weUpOne ]\n")

    //************* div() ******************
    val h = 10
    val p = 5.5

    val divide = h.toDouble().div(p)
    println("<|roundIt() method 0.01|>\n" +
            "h.toDouble().div(p) = [ ${divide.roundIt()} ]")

    println("------------------------------")

    // Above to the nearest .01 or below to the nearest .1
    println("<|rdNearPlace() method 0.1|>\n" +
            "h.toDouble().div(p) = [ ${divide.rdNearPlace()} ]\n")

    //************* rem() ******************
    val r = 15
    val v = 10

    // Returns a remainder of two values divided
    val remainder = r.rem(v)
    println("r.rem(v) = [ $remainder ]")
    //==============================================
    "Strings-StringMethods".codeDiv()

    var aString = "Alias111"
    println(aString)

    val strObj = String()
    println("'$strObj'")

    val charArray = aString.toCharArray()
    println("An array of characters:=> ${charArray.toList()}")
    // Give me the integer value of each character
    val byteArray = aString.toByteArray()
    println("A byte array of each character:=>\n${byteArray.toList()}")

    // A handy trick but makes the call emuttable
    aString += " and Welcomes!"
    println("\naString += [ $aString ]")

    // The length property: No need for the parenthesis '()'
    // since it is a property, not a method.
    val len = aString.length
    println("\naString.length = [ $len ]")

    print("\n<|Iterating through aString|>:\n")
    // Iterating through the string
    for (i in 0 until len) {
        val c = aString.get(index = i)
        print(c)
    }
    println()

    // Parsing aString
    val t = aString.indexOf("W")
    val sub = aString.substring(t)
    println(sub)

    // Equals function
    val aStr = aString.toUpperCase() // Creates a copy
    // To ignore that aString was mutated toUpperCase()
    // pass a second argument of "true" in the .equals() method
    val matches = aString.equals(aStr, true)
    println("\nAre the two string objects equal: [ $matches ]")
    //==============================================
    "StringBuilder-Class".codeDiv()

    // Does not create multiple string objects because
    // we are using a string builder
    val builder = StringBuilder("To be or not to be\n")
    // Appending/Adding to our builder
            .append("That is the question!\n")
            .append("Whether tis nobler in the mind\n")
            .append("to suffer the slings & arrows")

    val result = builder.toString()
    println("[ $result ]")
    //==============================================
    "Const-Keyword Use-case".codeDiv()

    println("The color is:=> [ $RED ]")
    println("The color is:=> [ $BLUE ]")
}
