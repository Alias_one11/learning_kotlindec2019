package kt_essentials.managedata_collections

import kt_essentials.basics.codeDiv
import kt_essentials.create_custom_classes.dataclasses.ClothingItem
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Working With Unordered-Sets".codeDiv()
    //==============================================
    "Mutable-Sets".printHeader()

    val colorSet = mutableSetOf("Red", "Green", "Blue")
    println("Mutable set: $colorSet")

    // Adding & removing from the set.
    colorSet.add("Purple")
    colorSet.remove("Red")
    println("\n<|Added new value & removed old value|>:\n$colorSet")

    /** You cannot remove an item from a set by index.
     *  Because the order of items, isn't guaranteed.
     *  You can pass a Set into a mutable list though
     *  & then use all the list functionality to your liking.*/
    val passMeTheSet = colorSet.toMutableList()
    passMeTheSet.removeAt(0)
    println("\n" + "<|Passing a Set to a mutable|>\n" +
            "<|list & removing by index|>: $passMeTheSet\n")

    //==============================================
    "Sets & Data-Classes".printHeader()

    val dataClassTypeSet = mutableSetOf<ClothingItem>()

    /** [run]: The context object is available as a receiver (this). The
     * return value is the lambda result. run does the same as "with" but
     * invokes as "let" - as an extension function of the context object.
     * run is useful when your lambda contains both the object initialization
     * and the computation of the return value.*/
    dataClassTypeSet.run {
        // Working with the constructor of "ClothingItem"
        add(ClothingItem("T-Shirt", "XL", 14.99))
        add(ClothingItem("Blue-Jeans", "36L*36W", 39.99))
    }
    println("\n" + "<|Using a mutable set, with a data-class object|>\n" +
            "<|& then using a run function|>:\n$dataClassTypeSet\n")
}