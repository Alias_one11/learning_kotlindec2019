package kt_essentials.managedata_collections

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Working With List".codeDiv()
    //==============================================
    /** List, Sets, & Maps, come in
     *  mutable or emutable versions. */
    "List".printHeader()

    // Emutable list declaration
    val colorList = listOf("Red", "Green", "Blue")
    // List come with a built in "toString()"
    // which will output it formatted
    println("Emutable List:\n$colorList")

    // Checking the size
    println("\nNumber of colors: [ ${colorList.size} ]")

    // Working with mutable List &
    // there is no need to specify size.
    val mutateColors = mutableListOf<String>()
    mutateColors.add("Orange")
    mutateColors.add("Violet")
    mutateColors.add("Tan")

    println("\nMutable List:\n$mutateColors")




}