package kt_essentials.managedata_collections

import kt_essentials.basics.*
import kt_essentials.create_custom_classes.dataclasses.ClothingItem
import kt_essentials.programflow.functions.*

fun main()
{
    //==============================================
    "Working With Maps<Key, Value>".codeDiv()
    //==============================================
    "EMutable-Map".printHeader()

    /** [Pair]:Represents a generic:type<'T','K'> pair of two values.
     *  There is no meaning attached to values in this class, it can
     *  be used for any purpose. Pair exhibits value semantics, i.e.
     *  two pairs are equal if both components are equal.*/
    val colorMap =
        mapOf<String, Long>(
            Pair("Red", 0xFF0000),
            Pair("Green", 0x00FF00),
            Pair("Blue", 0x0000FF)
        )
    println("<|Using the 'Pair' Generic class|>\n $colorMap")

    /** [Pair] & [to] essentially work the same */
    val colorMap2 =
        mapOf<String, Long>(
            "Red" to 0xFF0000,
            "Green" to 0x00FF00,
            "Blue" to 0x0000FF
        )
    println("\n<|Using the 'to' Generic function|>\n$colorMap2")

    //==============================================
    "Mutable-Map || Dynamic-Collection".printHeader()

    /** [apply]: The context object is available as a receiver (this).
     *  The return value is the object itself. Use apply for code blocks
     *  that don't return a value and mainly operate on the members of
     *  the receiver object. The common case for apply is the object
     *  configuration. Such calls can be read as “apply the following
     *  assignments to the object.”*/
    val stateMap = mutableMapOf<String, String>()
            .apply {
                put("CA", "Sacramento")
                put("OR", "Salem")
                put("WA", "Olympia")
            }
    println("<|Mutable Map using the apply function|>\n$stateMap")

    //==============================================
    "Looping Through the Map".printHeader()

    println("[JAVA]: (|Way to iterate through the map|)")
    /** [JAVA]: Way to iterate through the map*/
    for (state in stateMap.keys) {
        println("State: [ $state ]\nCapital: [ ${stateMap.get(key = state)} ]")
    }

    println("\n[KOTLIN]: (|Way to iterate through the map|)")
    /** [KOTLIN]: Way to iterate through the map*/
    for ((state, capital) in stateMap) {
        println("State: [ $state ]\nCapital: [ $capital ]")

    }

    //==============================================
    "Managing A Shopping Cart With A Map".printHeader()

    /** [MUTABLEMAPOF<CLOTHINGITEM, INT>()]: the int represents
     * the quantity purchased of each item in the cart. */
    val shoppingCart = mutableMapOf<ClothingItem, Int>()
            .apply {
                put(ClothingItem("Gucci-Shirt", "XL", 599.99), 2)
                put(ClothingItem("Suit", "Slim-Fit", 2967.99), 1)
                put(ClothingItem("Tie", "Wide", 178.99), 4)
            }

    // Calling my checkout function
    checkOut(shoppingCart)
}
