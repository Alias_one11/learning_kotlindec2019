package kt_essentials.managedata_collections

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Working With Arrays".codeDiv()
    //==============================================
    // Array in kotlin are usually
    // created with built in methods.
    "Basic-Array".printHeader()

    val arr1: Array<String> = arrayOf("Red", "Green", "Blue")
    /** The order of the objects in the
     *  array will always be kept in the
     *  order they were declared */
    for (element in arr1) {
        println(element)
    }
    //==============================================
    // arrayOf can also be used with
    // mixed types to create an array
    "Mixed:Type-Array".printHeader()

    val mixed: Array<Any> = arrayOf("One", 2, "Three", 4)
    for (element in mixed) {
        println(element)
    }
    //==============================================
    /** If you want to create an array of
     *  a particular size, but want the
     *  value to be null, until you add none
     *  null values use `arrayOfNulls<"type">("size")`*/
    "Null-Array & Type-Size".printHeader()

    val nullArray: Array<String?> = arrayOfNulls(3)
    for (element in nullArray) {
        println(element)
    }

    println("\n<|[ WITH ELEMENTS ADDED TO nullArray ]|>")
    // Adding elements to the null array
    nullArray[0] = "Hot"
    nullArray[1] = "Cold"
    nullArray[2] = "Mild"
    // Looping through the array with
    // the new values that were added
    for (element in nullArray) {
        println(element)
    }

    //==============================================
    "Arrays Of Specific-Types".printHeader()

    val intArr = intArrayOf(8, 17, 12, 45, 1)
    for (num in intArr) {
        print("$num\t")
    }

    //==============================================
    "Alpha-numerically-Descending order".printHeader()
    // Sorting Alpha-numerically
    println("<|[ Sorting Alpha-numerically ]|>")

    intArr.sort()
    for (num in intArr) {
        print("$num\t")
    }

    println("\n<|[ Sorting in Descending order ]|>")
    // Sorting in Descending order
    // Looping from back to front
    val arrBackwd = intArr.sortedArrayDescending()
    for (num in arrBackwd) {
        print("$num\t")
    }
}