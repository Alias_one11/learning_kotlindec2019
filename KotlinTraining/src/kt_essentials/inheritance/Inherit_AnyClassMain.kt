package kt_essentials.inheritance

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Inheritance-Class-Any".codeDiv()
    //==============================================
    "Any-Example".printHeader()

    val x: Any = Any()
    println(x)

    // Creating an instance of SuperClass
    val superC = SuperClass(42)
    println(superC)

    // Creating an instance of SubClass
    val subC = SubClass(17)
    println(subC)

    "Multiply Function overriding".printHeader()

    println("superC.multi(100) = [ ${superC.multi(100)} ]")
    println("subC.multi(100) = [ ${subC.multi(100)} ]")


}
/** [PARENTCLASS] */
open class SuperClass(anInt: Int) {
    /** The value is protected but it is available
     *  for this class and any of it's sub-classes */
    protected val _anInt = anInt

    override fun toString(): String {
        return "(|${this::class.simpleName}|):=> anInt: [ $_anInt ]"
    }

    open fun multi(factor: Int): Int = _anInt * factor

}
/** [CHILDCLASS] */
class SubClass(anInt: Int): SuperClass(anInt) {

    override fun toString(): String {
        return "(|${this::class.simpleName}|):=> anInt: [ $_anInt ]"
    }

    override fun multi(factor: Int): Int {
        return super.multi(factor).times(factor)
    }
}