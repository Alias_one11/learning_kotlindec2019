package kt_essentials.inheritance.interfaces

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader

fun main()
{
    //==============================================
    "Interfaces".codeDiv()
    //==============================================
    "Testing Our Interface".printHeader()

    val fordMustang = Car()
    with(fordMustang, { ->
        iGo()
        // Let's go fast, shall we?😎🇩🇴
        iGetMpg(160)
        // Let's test the anti-lock BRAKES
        iStop()
    })


}


