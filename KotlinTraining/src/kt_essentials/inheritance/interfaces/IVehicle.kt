package kt_essentials.inheritance.interfaces

interface IVehicle {
    val iMakeName: String
    fun iGo(): Unit = println("::Interface:: iGo():=> [Vroom..Vroom...]")
    fun iStop(): Unit = println("::Interface:: iStop():=> [Screech!!!!..]")
    fun iGetMpg(spd: Int): Int
    fun iGetDoor(): Int
}