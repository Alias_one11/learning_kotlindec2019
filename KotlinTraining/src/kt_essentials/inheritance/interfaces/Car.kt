package kt_essentials.inheritance.interfaces

class Car: IVehicle {
    override val iMakeName: String = "Ford-Mustang"

    override fun iGetDoor(): Int = 2

    override fun iGetMpg(spd: Int): Int {
        println("The [$iMakeName] is going [$spd] MPH..")
        return spd
    }

}