package kt_essentials.create_custom_classes.dataclasses

data class Person(private val fName: String, private val lName: String) {

    val fullName: String
    get() = "$fName $lName"

}