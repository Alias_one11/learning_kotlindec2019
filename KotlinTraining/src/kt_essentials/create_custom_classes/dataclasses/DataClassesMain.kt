package kt_essentials.create_custom_classes.dataclasses

import kt_essentials.basics.codeDiv
import kt_essentials.programflow.functions.printHeader
import java.text.NumberFormat
import java.util.*

fun main()
{
    //==============================================
    "Data-Classes".codeDiv()
    //==============================================
    "Primary-Constructor-Code".printHeader()

    val item = ClothingItem("T-Shirt", "XL", 19.99)
    // Automatic toString() from the data class
    println(item)

    item.price = 15.99
    // Will output with the new price since
    // now "val price" is "var price"
    println(item)
    //==============================================

    "Secondary-Constructor-Code".printHeader()

    val item2 = ClothingItem("L", 14.99)
    println(item2)

    println("Item type:=> [ ${item2.type} ]")
    // Using our 10% discount on the price of the t-shirt
    item2.price = 10.0
    println("Item price:=> [ Simple toString(): ${item2.price} ]")

    "Person-Class||Java-Currency-Class".printHeader()

    /** FROM THE JAVA LIBRARY OR FROM THE PLATFORM!:
     *  "import java.text.NumberFormat" Console
     *  output of the price will be formatted
     *  for currency "$0.00" */
    val formatter = NumberFormat.getCurrencyInstance()
    println("Item price:=> [ USA Currency: ${formatter.format(item2.price)} ]")
    // To change the currency
    Locale.setDefault(Locale.GERMANY)
    val formatter2 = NumberFormat.getCurrencyInstance()
    println("Item price:=> [ Euro Currency: ${formatter2.format(item2.price)} ]")

    // Creating a person instance
    val person = Person("Sami", "Martinez")
    println("That is [ ${person.fullName} ]")
}