package kt_essentials.create_custom_classes.dataclasses

/** A data class must have at least one field.
 *  Instantly by creating a data class, we receive
 *  some automatic functionality. Automatic:
 *  toString(), getters and setters. If you
 *  do not need any other implementation, then
 *  what is in the constructor, you can remove
 *  the curly braces "{}" and everything is all good.*/
data class ClothingItem(
        private var _type: String?,
        val size: String,
        private var _price: Double) {

    // Initializing our properties
    init {
        _type = _type?.toUpperCase() ?: "UNKNOWN"

    }

    var type: String? = _type
    get() = field ?: "UNKNOWN"
    // Setting a 10% discount on the price
    var price = _price
    set(value) {
        field = value * 0.9
    }

    /** Each secondary constructor must chain to the
     *  constructor before it. the way that is done is
     *  with the "this()" call, that passes in each
     *  field or a null value to the properties in
     *  the primary constructor. The null value has to be,
     *  for example "foo: String?" or optional type,
     *  for the passing of null to not give an error
     *  and make that field an optional parameter. */
    constructor(size: String, price: Double): this(null, size, price)
}

