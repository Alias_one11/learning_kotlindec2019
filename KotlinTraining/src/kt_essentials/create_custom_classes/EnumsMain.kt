package kt_essentials.create_custom_classes

import kt_essentials.basics.*
import kt_essentials.create_custom_classes.MathLib.Companion.calcValEnum
import kt_essentials.create_custom_classes.MathLib.Companion.getInput

fun main()
{
    //==============================================
    "Using Enums".codeDiv()
    //==============================================
    try {
        val num1 = getInput("Number #1: ")
        val num2 = getInput("Number #2: ")

        print("Select an operation [ ${MathLib.OPERATIONS} ]")

        val result: Double? = calcValEnum(num1, num2)

        println("The answer is:=> [ ${result?.roundIt()} ]")

    } catch (e: NumberFormatException) {
        println("${e.message} is not a number...")

    } catch (e: Exception) {
        println(e.message)
    }

}

