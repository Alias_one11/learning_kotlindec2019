package kt_essentials.create_custom_classes

import kt_essentials.basics.codeDiv
import kt_essentials.create_custom_classes.MathLib.Companion.getInput

fun main()
{
    //==============================================
    "Creating Class-Instance-Members".codeDiv()
    //==============================================

    try {
        // The class does not have a constructor
        // but a default constructor with no
        // argument is created for you
        val mathLib = MathLib()

        /** While the user inputs a number you can keep
         *  adding numbers to the loop but if you enter
         *  a string then an exception will be thrown. */
        while (true) {
            val num = getInput("Enter a number: ")
            mathLib.addValue(num)

            println("Current total:=> [ ${mathLib.runningTotal} ]")
        }

    } catch (e: NumberFormatException) {
        println("${e.message} is not a number...")

    } catch (e: Exception) {
        println(e.message)
    }
}