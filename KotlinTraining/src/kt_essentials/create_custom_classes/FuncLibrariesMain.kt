package kt_essentials.create_custom_classes

import kt_essentials.basics.codeDiv
import kt_essentials.create_custom_classes.MathLib.Companion.getInput

fun main()
{
    //==============================================
    "Function-Libraries".codeDiv()
    //==============================================
    try {
        /**import KtEssentials.create_custom_classes.MathLib.Companion.getInput
         * so that you don't have to declare it "MathLib.getInput"
         * it is the same as statically importing "Console.Writeline
         * in C# and other statically typed languages "*/
        val num1 = getInput("Number #1: ")
        val num2 = getInput("Number #2: ")


        val result: Double = MathLib.addVal2(num1, num2)
        println("The answer is:=> [ $result ]")

    } catch (e: NumberFormatException) {
        println("${e.message} is not a number...")

    } catch (e: Exception) {
        println(e.message)
    }
}

