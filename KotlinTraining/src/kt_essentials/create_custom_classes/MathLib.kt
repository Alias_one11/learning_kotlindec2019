package kt_essentials.create_custom_classes

class MathLib {

    var runningTotal = 0.0

    fun addValue(value: Double) {
        runningTotal += value
    }

    /** Any members placed inside a "companion"
     *  object can be accessed by the "MathLib" class.*/
    companion object {
        const val OPERATIONS = "+, -, *, /, %"

        /** @JvmStatic: If you use this annotation, the compiler will
         *  generate both a static method in the enclosing class of
         *  the object and an instance method in the object itself.*/
        @JvmStatic fun addVal2(param1: Double, param2: Double) = param1 + param2
        @JvmStatic fun minusVal2(param1: Double, param2: Double) = param1 - param2
        @JvmStatic fun timesVal2(param1: Double, param2: Double) = param1 * param2
        @JvmStatic fun divideVal2(param1: Double, param2: Double) = param1 / param2
        @JvmStatic fun modVal2(param1: Double, param2: Double) = param1 % param2

        fun calcValEnum(num1: Double, num2: Double): Double? {
            return when (readLine()) {
                Operation.ADD.operator -> addVal2(num1, num2)
                Operation.SUBTRACT.operator -> minusVal2(num1, num2)
                Operation.MULTIPLY.operator -> timesVal2(num1, num2)
                Operation.DIVIDE.operator -> divideVal2(num1, num2)
                Operation.MOD.operator -> modVal2(num1, num2)
                else -> throw Exception("[ERROR: UNKNOWN OPERATOR]")
            }
        }

        fun getInput(prompt: String): Double {
            print(prompt)
            // creating a readLine() object for
            // user input then casting it to a double
            val str: String? = readLine()
            // Big decimal is a lot more precise
            val num = str!!.toBigDecimal()
            return num.toDouble()
        }
    }
}