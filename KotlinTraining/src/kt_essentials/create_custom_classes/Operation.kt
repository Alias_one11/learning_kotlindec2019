package kt_essentials.create_custom_classes

enum class Operation(val operator: String) {
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/"),
    MOD("%")
}